define(["lodash", "backbone"], function(_, Backbone) {
	
	return  Backbone.View.extend({

		events: {
			"click .right-arrow": "moveRight",
			"click .left-arrow": "moveLeft",
			"click .buttons button": "buttonClick"
		},
		
		el: ".carousel",

		numSlides: 0,
		index: 0,

		reset: function() {

			this.$(".carousel-content").css({width: "auto"});
			this.$(".carousel-content li").width("100%");
			this.$(".carousel-content ul").width("100%").css({left: 0});

		},

		moveRight: function() {

			this.flipTimeoutStart();

			this.index = (this.index+1) % this.numSlides;
			this.highlightButton();

			var width = this.$(".carousel-content").width();

			this.$(".carousel-content").css({width: width+"px"});
			this.$(".carousel-content li").width(width);
			this.$(".carousel-content ul").width(width*2);

			this.$("ul").animate({
				left: width * -1
			}, {
				specialEasing: {
					width: 'linear',
					height: 'easeOutBounce'
				},
				duration: 500,
				complete: _.bind(function() {

					var li = this.$(".carousel-content li:first-child").detach();

					this.$(".carousel-content ul").append(li);

					this.reset();

				}, this)
			});

		},

		moveLeft: function() {

			this.flipTimeoutStart();

			this.index = (this.index - 1 < 0 ? this.numSlides - 1 : this.index - 1);
			this.highlightButton();

			var width = this.$(".carousel-content").width();

			var li = this.$(".carousel-content li:last-child").detach();

			this.$(".carousel-content ul").prepend(li);

			this.$(".carousel-content").css({width: width+"px"});
			this.$(".carousel-content li").width(width);
			this.$(".carousel-content ul").width(width*2).css({left: width*-1});

			this.$("ul").animate({
				left: 0
			}, {
				specialEasing: {
					width: 'linear',
					height: 'easeOutBounce'
				},
				duration: 500,
				complete: _.bind(function() {

					this.reset();

				}, this)
			});
		},

		buttonClick: function(evt) {

			this.moveTo(this.$(".buttons button").index(evt.currentTarget));
		},

		moveTo: function(newIndex) {

			this.flipTimeoutStart();

			var currentIndex = this.index,
				distance = (newIndex - currentIndex > 0 ? newIndex - currentIndex : currentIndex - newIndex),
				width = this.$(".carousel-content").width();

			this.index = newIndex;
			this.highlightButton();


			this.$(".carousel-content").css({width: width+"px"});
			this.$(".carousel-content li").width(width);
			this.$(".carousel-content ul").width(width * (distance + 1));


			if(currentIndex < newIndex) {

				this.$("ul").animate({
					left: width * distance * -1
				}, {
					specialEasing: {
						width: 'linear',
						height: 'easeOutBounce'
					},
					duration: 500,
					complete: _.bind(function() {
						
						_(distance).times(function() {
							var li = this.$(".carousel-content li:first-child").detach();
							this.$(".carousel-content ul").append(li);
						}, this);

						this.reset();

					}, this)
				});
				

			} else if(currentIndex > newIndex) {

				this.$(".carousel-content ul").css({left: width*distance*-1});

				_(distance).times(function() {
					var li = this.$(".carousel-content li:last-child").detach();
					this.$(".carousel-content ul").prepend(li);
				}, this);

				this.$("ul").animate({
					left: 0
				}, {
					specialEasing: {
						width: 'linear',
						height: 'easeOutBounce'
					},
					duration: 500,
					complete: _.bind(function() {

						this.reset();

					}, this)
				});

			}

		},

		highlightButton: function() {
			
			this.$(".buttons button").removeClass("active");

			this.$(".buttons button:eq("+this.index+")").addClass("active");

		},

		initialize: function() {

			this.numSlides = this.$(".carousel-content li").length;

			this.highlightButton();

			this.flipTimeoutStart();

		},

		flipTimeoutStart: function() {
			
			var milliseconds = 5000;

			if(this.timeout)
				clearTimeout(this.timeout);

			this.timeout = setTimeout(_.bind(function() {
				this.flipTimeoutStart(milliseconds);
				this.moveRight();
			}, this), milliseconds);
	
		}

	});

});