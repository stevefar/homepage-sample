/**
*
*
* @uses jquery
* @uses 
**/
define(["bootstrap-tooltip"], function($) {
	// Show Tooltips.
	var rightColumn = $("#rightColumn");
	rightColumn.find("a[title], a[data-original-title]").each(function() {

		$(this).tooltip({placement: 'left'});

	}).click(function(e) {
	
		/** Headline Tracking **/
		// TODO e2e test.
	
		var type = 'rss',
			id = $(this).attr("headlineid"),
			url = e.currentTarget.href;
	
	    (new Image()).src="//www."+window.config.rootdomain+"/client/redirect.php?R=true&url="+url+"&from="+type+"&id="+id; 
	
	});

	// Hack to get right column to bottom of page
	// TODO confirm and unit test
	if(rightColumn.length) {
		var height_of_column = rightColumn.outerHeight() + rightColumn.offset().top;
		var height_of_page = $(window).height();
		rightColumn.css({minHeight: rightColumn.height() + height_of_page - height_of_column});	
	}
	
});