var express = require("express"),
	app = express();

// Jade view engine setup

app.set('views', __dirname);
app.set('view engine', 'jade');
app.locals.basedir = __dirname;
app.locals.headlines = JSON.parse(require("fs").readFileSync("./headlines.json").toString());
app.locals.facebook_likes = "106,929";
app.locals.twitter_followers = "101,518";
app.locals.news_feed = "35,797";
app.locals.homepage_stats = {
	million_text: "294.9",
	pageviews_today: "28,037",
	visitors_today: "7,248",
	countries: "121"
};
app.locals.config = {
	rootdomain: "socialpsychology.org"
};

// Routes 

app.get("/", function(req, res) {
	res.render("index");
});


// LESS Middleware

app.use(require('less-middleware')(__dirname));
app.use(express.static(__dirname));

// listen plus heroku ready

app.listen(process.env.PORT || 3001);