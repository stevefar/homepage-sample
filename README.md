# SPN Homepage

This repo contains the design update to Social Psychology Network's homepage ( previously http://www.legacy.socialpsychology.org/ ). The previous design used table layout and is based on a pixelated Photoshop file. This update uses new CSS features such as flexbox, but keeps Internet Explorer 9 compatibility by taking advantage of "display: table-cell". Many images were recreated or replaced using vector images to make the design "retina" or "HiDPI" ready (see below). The carousel functionality was created using Backbone and the headlines uses a customized bootstrap tooltip plugin.

Demo can be found here: http://protected-brushlands-4593.herokuapp.com/

## Top header

The header contains a [CSS gradient](http://www.colorzilla.com/gradient-editor/#f5f6e2+0,f5f6e2+64,74bc72+100&1+0,0.8+13,1+100;Custom) with a semi-transparent section in the middle (between the logo and right navigation). The image of the map is placed behind the gradient so that it pops out right in this area in the middle and is hidden when too close to the logo text or navigation.

The gradient was created with this tool: http://www.colorzilla.com/gradient-editor/#f5f6e2+0,f5f6e2+64,74bc72+100&1+0,0.8+13,1+100;Custom

### Generating vector images with Raphaeljs

SVG files were made inside the browser with Raphaeljs. See the code snippets for each SVG image below.

##### img/template/topbarSwoosh.svg
http://plnkr.co/edit/TOY9I8LvJFfl9HsCNCye?p=preview

##### img/template/leftGrad.svg
http://plnkr.co/edit/DuVUQtf74xJDKlHDwy1e?p=preview

##### img/template/rightColumnSwoosh.svg
http://plnkr.co/edit/7jt01LkikDMFGn2YwR8j?p=preview

##### img/template/rightTile.svg
http://plnkr.co/edit/HcJe2yTpbBZ1aEEpga8l?p=preview

##### img/template/top.svg
http://plnkr.co/edit/3Y9V6tTHPy9Nh0MZjbBN?p=preview

##### img/template/leftTile.svg
http://plnkr.co/edit/IK1cTqy2tVb88rTvs089?p=preview



## Running on local machine

    $ npm install
    $ node app

Now you can visit http://localhost:3001/