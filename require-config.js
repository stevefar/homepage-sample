require.config({
	shim: {
		jquery: { exports: '$' },
		lodash: { exports: '_' },
		backbone: { deps: ["lodash"], exports: "Backbone" },
		"bootstrap-tooltip": 	{exports: '$', deps: ['jquery']}
	},
	paths: {
		lodash: "bower_components/lodash/dist/lodash",
		underscore: "bower_components/underscore/underscore",
		jquery: "bower_components/jquery/dist/jquery",
		backbone: "bower_components/backbone/backbone",
		"bootstrap-tooltip": 	"bower_components/bootstrap3/js/tooltip"
	}
	
});